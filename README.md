# Woltair

## Name
woltair_install.yml

## Description
This playbook is used to do following:
- install nginx and htop on target machine
- set hostname of target machine to "server1"
- set ssh autorization and allow root login only remotely via port 65022
- set nginx to listen on test.example.com
- copy woltair_index.html to target machine

## Usage
ansible-playbook woltair_install.yml -b

## Authors comments
Playbook was tested multiple times during creation, aka it should work and not end with random error. No remote machine was available during creation, so localhost was used, but I dont think it was limiting factor for the exercise. There are however left some blank spaces and hashtag marks, that would not be used in production version.

Some known issues about the playbook, that would need to be fixed if this was not exercise:
- ssh configuration steps are made on general / serverwide level, more exact specification ("set this for root only") would probably be needed in production version
- /etc/ssh/ssh_config steps assume all lines (aka "PubkeyAuthentication") are present, which is not always true, so there are checks missing ("if line is not present, then...")
- nginx configuration might not be correct (nginx_conf.j2 file), but i tried my best...

I also added handy_commands.sh file with some commands that were used during creation of this playbook.

