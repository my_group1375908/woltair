# pip installation needed for further ansible installation
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py

# ansible instalation
python3 -m pip install --user ansible

# fixing PATH variable to make ansible work
vi ~/.bashrc
export PATH="/home/marek/.local/bin:$PATH"

# check ansible works
ansible --version

# under root, added sudo
usermod -aG sudo marek

# ssh key generation
ssh-keygen -t rsa

# ssh configuration check
cat /etc/ssh/ssh_config

# ssh installation on localhost
sudo apt-get install openssh-server

# shh service status
sudo service ssh status

# hostname check
echo $(hostname)

# nginx status
sudo systemctl status nginx
